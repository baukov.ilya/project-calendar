import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { CalendarModel } from '../../models/calendar.model';
import { RootState } from '../store';

export interface CalendarState {
    calendar: CalendarModel;
    isLoading: boolean;
    isError: boolean;
}

export const initialState: CalendarState = {
    calendar: {
        sprints : [],
        project_dates: [],
    },
    isLoading: false,
    isError: false
}

export const calendarSlice = createSlice({
    name: "calendar",
    initialState,
    reducers: {
        fetchCalendarAction: (state, action: PayloadAction<number>) => {
            state.isLoading = true;
        },
        fetchCalendarSuccessAction: (state, action: PayloadAction<CalendarModel>) => {
            state.isLoading = false;
            state.isError = false;

            state.calendar = action.payload;
        },

    }
});

export const {
    fetchCalendarAction,
    fetchCalendarSuccessAction,
} = calendarSlice.actions;

export default calendarSlice.reducer;

export const selectCalendar = (state: RootState) => state.calendar;
