import { put, takeLatest, call, delay } from "redux-saga/effects";
import {fetchCalendarAction, fetchCalendarSuccessAction} from './calendar.slice'
import { CalendarModel } from '../../models/calendar.model';

export const FETCH_CALENDAR = 'FETCH_CALENDAR';
export const URL = 'https://spreadsheets.google.com/feeds/cells/' + process.env.REACT_APP_GOOGLE_SHEETS_CODE?.split("/")[5] + "/";
console.log(process.env.REACT_APP_GOOGLE_SHEETS_CODE?.split("/")[5])
const fetchCalendarRequestAsync = ( page?: number): Promise<CalendarModel> => {
   return fetch(URL + page + "/public/full?alt=json")
        .then((response) => {
            return response.json();
        })
        .then((data) => {
            const info = data.feed.entry
            const array = info.map((item: any) => {
                return({
                    a1: item.title.$t,
                    b1: item.content.$t,
                })
            })
            let sprint = {
                number: "",
                startDate: "",
                endDate: "",
                status:"",
                planningResults: "",
                actualResults:""
            }
            let project_date = {
                name: "",
                startDate: "",
                endDate: ""
            }
            let array1 = []
            let array2 = []
            let startnum = 9
            if(array[8].a1[0] == "A")
                startnum = 8
            else if(array[7].a1[0] == "A")
                startnum = 7
            else if(array[6].a1[0] == "A")
                startnum = 6
            let flag1: boolean = false;
            let flag2: boolean = false;
            for (let i = startnum; i < array.length; i++){
                if(array[i].a1[0] == "A") {
                    if (flag1) {
                        let sprint_item = {...sprint}
                        array1.push(sprint_item)
                        sprint = {
                            number: "",
                            startDate: "",
                            endDate: "",
                            status:"",
                            planningResults: "",
                            actualResults:""
                        }
                    }
                    flag1 = true;
                    sprint.number = array[i].b1
                }
                if(array[i].a1[0] == "B")
                    sprint.startDate = array[i].b1
                if(array[i].a1[0] == "C")
                    sprint.endDate = array[i].b1
                if(array[i].a1[0] == "D")
                    sprint.status = array[i].b1
                if(array[i].a1[0] == "E")
                    sprint.actualResults = array[i].b1
                if(array[i].a1[0] == "F") {
                    sprint.planningResults = array[i].b1
                }
                if(array[i].a1[0] == "I") {
                    if (flag2) {
                        let project_date_item = {...project_date}
                        array2.push(project_date_item)
                        project_date = {
                            name: "",
                            startDate: "",
                            endDate: ""
                        }
                    }
                    flag2 = true;
                    project_date.name = array[i].b1
                }
                if(array[i].a1[0] == "J")
                    project_date.startDate = array[i].b1
                if(array[i].a1[0] == "K"){
                    project_date.endDate = array[i].b1
                }
            }
            if(sprint.number !== "" && sprint.startDate !== "" && sprint.endDate != "") {
                let sprint_item = {...sprint}
                array1.push(sprint_item)
            }
            if( project_date.startDate !== "" && project_date.endDate !== "") {
                let project_date_item = {...project_date}
                array2.push(project_date_item)
            }
            console.log(array1)
            console.log(array2)
            return {
                sprints: array1,
                project_dates: array2
            }
        });
}


export function* fetchCalendarAsync(action: any) {

    yield put(fetchCalendarAction(action.payload));
    yield delay(500);
    let response: CalendarModel = yield call(() => fetchCalendarRequestAsync(action.payload));
    yield put(fetchCalendarSuccessAction(response));

}

export function* watchFetchCalendarRequest() {
    yield takeLatest(FETCH_CALENDAR, fetchCalendarAsync);
}
