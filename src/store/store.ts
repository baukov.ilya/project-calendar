import storage from 'redux-persist/lib/storage';
import createSagaMiddleware from "redux-saga";
import { persistReducer, persistStore } from 'redux-persist';
import saga from './sagas';

import { configureStore, getDefaultMiddleware, combineReducers } from '@reduxjs/toolkit';
import calendar from './calendar/calendar.slice';
import googleLists from './google-lists/google-lists.slice';
import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux';

const sagaMiddleware = createSagaMiddleware();

const middleware = [
    ...getDefaultMiddleware({ thunk: false }),
    sagaMiddleware
];

const persistConfig = {
    key: 'root',
    version: 1,
    storage,
    whitelist: ['appInfo'],
};

const persistedReducer = persistReducer(
    persistConfig,
    combineReducers({
        calendar,
        googleLists
    })
);

export const store = configureStore({
    reducer: persistedReducer,
    middleware,
    preloadedState: {},
    enhancers: [],
});

sagaMiddleware.run(saga);

export const Persistor = persistStore(store);

export type RootState = ReturnType<typeof store.getState>;

export type AppDispatch = typeof store.dispatch;

export const useAppDispatch = () => useDispatch<AppDispatch>();

export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;


