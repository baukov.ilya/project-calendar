import { all } from 'redux-saga/effects';
import { watchFetchCalendarRequest } from './calendar/calendar.saga';
import { watchFetchGoogleListsRequest } from './google-lists/google-lists.saga';

export default function* rootSaga() {
    yield all([
        watchFetchCalendarRequest(),
        watchFetchGoogleListsRequest()
    ]);
}
