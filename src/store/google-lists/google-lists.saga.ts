import { put, takeLatest, call, delay } from "redux-saga/effects";
import {fetchGoogleListsAction, fetchGoogleListsSuccessAction} from './google-lists.slice'

export const FETCH_GoogleLists = 'FETCH_GoogleLists';
export const HANDLE_PageSwitch = 'HANDLE_PageSwitch';
export const URL = 'https://spreadsheets.google.com/feeds/worksheets/'+ process.env.REACT_APP_GOOGLE_SHEETS_CODE?.split("/")[5] + '/public/full?alt=json';

const fetchGoogleListsRequestAsync = (): Promise<any> => {
    return fetch(URL)
        .then((response) => {
            return response.json();
        })
        .then((data) => {
            const info = data.feed.entry
            return info.map((item: any, index: any) => {
                const object = {
                    number: index,
                    item: item.title.$t
                }
                return(object)

            })
        });
}


export function* fetchGoogleListsAsync() {

    yield put(fetchGoogleListsAction());
    yield delay(500);
    let response: any = yield call(() => fetchGoogleListsRequestAsync());
    yield put(fetchGoogleListsSuccessAction(response));

}

export function* watchFetchGoogleListsRequest() {
    yield takeLatest(FETCH_GoogleLists, fetchGoogleListsAsync);
}
export function* watchHandle_PageSwitchRequest() {
    yield takeLatest(HANDLE_PageSwitch, fetchGoogleListsAsync);
}
