import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../store';
import {CalendarModel} from "../../models/calendar.model";

export interface googleListsState {
    googleLists: any[];
    isLoading: boolean;
    isError: boolean;
    curPage: number;
}

export const initialState: googleListsState = {
    googleLists: [],
    isLoading: false,
    isError: false,
    curPage:1
}

export const googleListsSlice = createSlice({
    name: "googleLists",
    initialState,
    reducers: {
        fetchGoogleListsAction: (state) => {
            state.isLoading = true;
        },
        fetchGoogleListsSuccessAction: (state, action: PayloadAction<any>) => {
            state.isLoading = false;
            state.isError = false;
            state.googleLists = action.payload
        },


    }
});

export const {
    fetchGoogleListsAction,
    fetchGoogleListsSuccessAction,
} = googleListsSlice.actions;

export default googleListsSlice.reducer;

export const selectGoogleLists = (state: RootState) => state.googleLists;
