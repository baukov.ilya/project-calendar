import React, { memo, useCallback } from 'react';
import { FETCH_CALENDAR } from '../store/calendar/calendar.saga';
import { selectCalendar } from '../store/calendar/calendar.slice';
import { useAppDispatch, useAppSelector } from '../store/store';
import Chart from "../components/chart/chart.component";
import moment from "moment";

export const ChartContainer = memo(() => {
    const { calendar: { sprints: cycles, project_dates: events}, isLoading} = useAppSelector(selectCalendar);

    const dispatch = useAppDispatch();

    const fetchCalendar = useCallback((page) => { dispatch({ type: FETCH_CALENDAR, payload: page}) }, [dispatch]);


    return <Chart
        loading={isLoading}
        events={events.map(event => ({
            ...event,
            startDate: moment(event.startDate, 'DD.MM.YYYY'),
            endDate: moment(event.endDate, 'DD.MM.YYYY'),
        })).sort((A,B) => {
            return A.startDate - B.startDate;
        })}
        cycles={cycles.map(cycle => ({
            ...cycle,
            startDate: moment(cycle.startDate, 'DD.MM.YYYY'),
            endDate: moment(cycle.endDate, 'DD.MM.YYYY'),
        })).sort((A,B) => {
            return A.startDate - B.startDate;
        })}
        fetchCalendar={fetchCalendar}
    />;

});
