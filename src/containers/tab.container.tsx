import React, {memo, useCallback, useEffect} from 'react';
import  TabsComponent  from '../components/tabs/tabs.component'
import { useAppDispatch, useAppSelector } from '../store/store';
import { FETCH_GoogleLists } from "../store/google-lists/google-lists.saga";
import { selectGoogleLists } from "../store/google-lists/google-lists.slice";
import {FETCH_CALENDAR} from "../store/calendar/calendar.saga";

export const TabsContainer = memo(() => {

    const { googleLists, isLoading } = useAppSelector(selectGoogleLists);

    const dispatch = useAppDispatch();
    const fetchGoogleLists = useCallback(() => { dispatch({ type: FETCH_GoogleLists}) }, [dispatch]);
    useEffect(() => {
        fetchGoogleLists();
    }, [])

    const fetchCalendar = useCallback((page) => { dispatch({ type: FETCH_CALENDAR, payload: page}) }, [dispatch]);



    return <TabsComponent
        loading={isLoading}
        googleLists={googleLists}
        fetchCalendar={fetchCalendar}
        />;

});
