import {Event} from "../models/event.model";
import moment, {Moment} from "moment";
import {Cycle, CycleStatus} from "../models/cycle.model";
import {range} from "./range.util";

export const transformEventsToSeriesData = (events: Event[], firstDay?: Moment, daysCount?: number): any[] => {
    const data: any[] = [];
    if (!firstDay || !daysCount) {
        return [];
    }
    events.forEach(({startDate: start, endDate: end, name}) => {
        const startDay: number = moment.duration(start.diff(firstDay)).asDays();
        const endDay: number = moment.duration(end.diff(firstDay)).asDays();
        data.push({
            x: startDay,
            y: 80,
            name,
            dataLabels: {
                enabled: true,
                formatter(): string {
                    // @ts-ignore
                    return this.point.name
                }
            },
            custom: {
                start: start.clone().format('DD.MM.YYYY'),
                end: end.clone().format('DD.MM.YYYY'),
            }
        });
        if (startDay != endDay) {
            const partCount: number = (endDay - startDay) / daysCount * 100;
            const part = partCount < 100 ? (endDay - startDay) / partCount : 10;
            range(1, partCount, 1).forEach((num: number) => {
                data.push({
                    x: startDay + part * num,
                    y: 80,
                    name,
                    marker: {
                        enabled: false
                    },
                    custom: {
                        start: start.clone().format('DD.MM.YYYY'),
                        end: end.clone().format('DD.MM.YYYY'),
                    }
                });
            })
        }
        data.push({
            x: endDay,
            y: 80,
            name,
            dataLabels: {
                enabled: true,
                formatter(): string {
                    // @ts-ignore
                    return this.point.name
                }
            },
            custom: {
                start: start.clone().format('DD.MM.YYYY'),
                end: end.clone().format('DD.MM.YYYY'),
            }
        });
        data.push({
            x: endDay,
            y: null
        });
    })
    return data;
}

export const transformBoundariesToMonthSeriesData = (startDate: Moment, endDate: Moment): Map<string, Moment[]> => {
    const months: Map<string, Moment[]> =  new Map<string, Moment[]>();
    let endOfFirstMonth = startDate.clone().endOf('month');
    months.set(endOfFirstMonth.clone().format('MMM YYYY'), [startDate, endOfFirstMonth.clone()]);
    let nextDate = startDate.clone()
    while (nextDate < endDate) {
        nextDate.add(1, 'M');
        if (nextDate.clone().add(1, 'M') >= endDate) {
            let startOfMonth = nextDate.clone().startOf('month');
            months.set(nextDate.clone().format('MMM YYYY'), [startOfMonth.clone(), endDate.clone()]);
        } else {
            let startOfMonth = nextDate.clone().startOf('month');
            let endOfMonth = nextDate.clone().endOf('month');
            months.set(nextDate.clone().format('MMM YYYY'), [startOfMonth.clone(), endOfMonth.clone()]);
        }
    }
    console.log('!! months: ', Array.from(months.entries())
        .map(month => [month[0], month[1][0].format('DD.MM.YYYY'), month[1][1].format('DD.MM.YYYY')]))
    return months;
}

export const transformCycleStatusToColor = (status: CycleStatus): string => {
    switch (status) {
        case 'Зачтен':
            return '#80EE7D';
        case 'Не зачтен':
            return '#F25C80';
        case 'Текущий':
            return '#FEFF86';
        case 'Не начат':
            return '#9E9FA3';
        case 'Каникулы':
            return '#fff';
        default:
            return ''
    }
}

export const transformCyclesToCyclesWithHolidays = (cycles: Cycle[]): Cycle[] => {
    const cyclesWithHolidays: Cycle[] = [];
    cycles.forEach((cycle, index) => {
        if (index === cycles.length - 1) {
            cyclesWithHolidays.push(cycle);
        } else {
            const daysCount: number = moment.duration(cycles[index + 1].startDate.diff(cycle.endDate)).asDays();
            cyclesWithHolidays.push(cycle);
            if (daysCount > 1) {
                cyclesWithHolidays.push({
                    number: -1,
                    startDate: cycle.endDate.clone().add(1, 'days'),
                    endDate: cycles[index + 1].startDate.clone().add(-1, 'days'),
                    status: 'Каникулы'
                })
            }
        }
    })
    return cyclesWithHolidays;
}
