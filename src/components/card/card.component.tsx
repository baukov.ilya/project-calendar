import React, {memo} from "react";
import {Card, Divider} from 'antd';
import styles from "./card.module.less"
import {CloseOutlined} from "@ant-design/icons";
import {Cycle} from "../../models/cycle.model";


interface CardComponentProps {
    readonly sprint: Cycle;
    readonly onClose: () => void;
}
const CardComponent = memo<CardComponentProps>(({sprint, onClose}) => {

    return (
        sprint && (
            <div className="site-card-border-less-wrapper" style={{
                boxShadow : " -1px 0px 10px 2px rgba(34, 60, 80, 0.2)",
                marginTop: "20px",
                marginRight: "20px"}
            }>
                <Card
                    className={styles.card__card}
                    title={`Цикл ${sprint.number}`}
                    bordered={false}
                    style={{ width: "100%" }}
                    extra={<CloseOutlined onClick={onClose} />}
                >
                    <p>
                        {sprint.status}
                    </p>
                    <Divider style={{fontWeight: "bold"}}>Планируемые задачи</Divider>
                    <p>
                        {sprint.actualResults}
                    </p>
                    <Divider style={{fontWeight: "bold"}}>Фактические результаты</Divider>
                    <p>
                        {sprint.planningResults}
                    </p>
                </Card>
            </div>
        )
    );
});

export default CardComponent;
