import React from 'react';
import styles from './app.module.less';
import {ResizeContext, useResizeProvider} from './resize.hook';
import {Col, Layout, Row} from "antd";
import Icon from "../ui/icon/icon.component";
import {TabsContainer} from "../../containers/tab.container";
import {ChartContainer} from "../../containers/chart.container";

const { Header, Footer, Content } = Layout;

const App: React.FC = () => {

    const width: number = useResizeProvider();

    return (
        <div className={styles.app}>
            <ResizeContext.Provider value={{ width }}>
                <Layout className={styles.app__layout}>
                    <Header className={styles.app__header}>
                        <Icon name="atom" className={styles.app__headerIcon}/>
                        <span>Project Calendar</span>
                    </Header>
                    <Content className={styles.app__content}>
                        <Row className={styles.app__contentContainer}>
                            <Col span={4} style={{ height: '100%'}}>
                                <TabsContainer />
                            </Col>
                            <Col span={20}>
                                <ChartContainer/>
                            </Col>
                        </Row>
                    </Content>
                    <Footer className={styles.app__footer}>(c) HSE 2021</Footer>
                </Layout>
            </ResizeContext.Provider>
        </div>
    );
};

export default App;
