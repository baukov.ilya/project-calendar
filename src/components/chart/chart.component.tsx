import React, {memo, useCallback, useEffect, useMemo, useRef, useState} from "react";
import Highcharts from 'highcharts';
import HighchartsMore from 'highcharts/highcharts-more'
import styles from './chart.module.less';
import HighchartsReact from 'highcharts-react-official';
import ReactResizeDetector from 'react-resize-detector';
import {SimpleMap} from "../../models/simple-map.model";
import {Cycle} from "../../models/cycle.model";
import {Event} from "../../models/event.model";
import moment, {Moment} from "moment";
import {useResize} from "../app/resize.hook";
import {
    transformBoundariesToMonthSeriesData,
    transformCycleStatusToColor, transformCyclesToCyclesWithHolidays,
    transformEventsToSeriesData
} from "../../utils/transform.util";
import EmptyLoader from "../common/empty-loader/empty-loader.component";
import { Col, Row} from "antd";
import CardComponent from "../card/card.component";
import {range} from "../../utils/range.util";
HighchartsMore(Highcharts)

export interface ChartProps {
    readonly cycles: Cycle[];
    readonly events: Event[];
    readonly fetchCalendar: (page: number) => void;
    readonly loading: boolean;
}

// @ts-ignore
window.moment1 = moment;

const Chart = memo<ChartProps>(({ cycles, events, loading}) => {
    const chartRef = useRef<any>(null);

    const [firstDay, setFirstDay] = useState<Moment>();
    const [daysCount, setDaysCount] = useState<number>(0);

    const [months, setMonths] = useState<Map<string, Moment[]>>(new Map<string, Moment[]>());

    const [activeCycle, setActiveCycle] = useState<Cycle | null>();

    const handlePieClick = useCallback((number: number) => {
        const cycle: Cycle | undefined = cycles.find(cycle => cycle.number === number);
        if (!cycle) {
            return;
        }
        setActiveCycle(cycle);
    }, [cycles]);

    useEffect(() => {
        const cyclesWithHolidays: Cycle[] = transformCyclesToCyclesWithHolidays(cycles);
        console.log('!! cyclesWithHolidays', cyclesWithHolidays);
    }, [cycles])

    const width: number = useResize();

    useEffect(() => {
        if (!cycles.length) {
            return;
        }
        setFirstDay(cycles[0].startDate);
        setDaysCount(moment.duration(cycles[cycles.length - 1].endDate.diff(cycles[0].startDate)).asDays() + 1)
        setMonths(transformBoundariesToMonthSeriesData(
            cycles[0].startDate,
            cycles[cycles.length - 1].endDate
        ));
        setActiveCycle(null);
    }, [cycles]);

    useEffect(() => {
        console.log('daysCount', daysCount);
    }, [daysCount]);

    const [containerHeight, setHeight] = useState<number>(0);
    const options = useMemo(() => ({
        chart: {
            height: containerHeight,
            polar: true
        },
        title: {
            text: ''
        },
        credits: {
            enabled: false
        },
        pane: {
            startAngle: 0,
            endAngle: 360
        },
        xAxis: {
            crosshair: true,
            visible: false,
            tickInterval: 5,
            min: 0,
            max: daysCount,
        },
        yAxis: {
            visible: false,
            height: '100%',
            min: 0,
            max: 100
        },
        plotOptions: {
            series: {
                pointStart: 0,
                pointInterval: 45
            },
            column: {
                pointPadding: 0,
                groupPadding: 0
            }
        },
        legend: {
            enabled: false
        },
        series: [
            {
                type: 'pie',
                center: ['50%', '50%'],
                innerSize: '50%',
                size: '100%',
                distance: 100,
                cursor: 'pointer',
                data: transformCyclesToCyclesWithHolidays(cycles).map(({startDate: start, endDate: end, number, status}) => {
                    return {
                        y: moment.duration(end.diff(start)).asDays() + 1,
                        name: number,
                        color: transformCycleStatusToColor(status),
                        custom: {
                            start: start.clone().format('DD.MM.YYYY'),
                            end: end.clone().format('DD.MM.YYYY'),
                            status
                        }
                    }
                }),
                dataLabels: {
                    formatter():number {
                        // @ts-ignore
                        return this.y > 5 && this.point.custom.status !== 'Каникулы' ? this.point.name : null;
                    },
                    color: '#ffffff',
                    distance: '-45%',
                    // enabled: width > 500
                },
                tooltip: {
                    headerFormat: '',
                    pointFormatter(): any {
                        // @ts-ignore
                        if (this.custom.status === 'Каникулы') {
                            return 'Каникулы :)'
                        }
                        // @ts-ignore
                        return `<b>Цикл ${this.name}</b><br />${this.custom.start} - ${this.custom.end}`
                    }
                },
                events: {
                    click(event: any): any {
                        handlePieClick(event.point.name);
                    }
                }
            },
            {
                type: 'pie',
                center: ['50%', '50%'],
                size: '45%',
                distance: 100,
                data: !!cycles ? Array.from(months).map(([month, [startDate, endDate]]) => {
                    // const value = moment.duration(endDate.diff(startDate)).asDays() + 1;
                    return {
                        y: moment.duration(endDate.diff(startDate)).asDays() + 1,
                        name: month,
                        color: '#7BB5EC',
                    }
                }) : [],
                tooltip: {
                    headerFormat: '',
                    pointFormatter(): any {
                        // @ts-ignore
                        return `<b>${this.name}</b>`
                    }
                },
                dataLabels: {
                    enable: false,
                    formatter():number {
                        // @ts-ignore
                        return this.y > 10 ? this.point.name.split(' ').join('<br />') : null;
                    },
                    color: '#ffffff',
                    distance: '-20%',
                    // enabled: width > 500
                }
            },
            {
                type: 'spline',
                name: 'Line',
                color: '#7BB5EC',
                enableMouseTracking: false,
                tooltip: {
                    headerFormat: '',
                    pointFormatter(): any {
                        // @ts-ignore
                        return `${this.name}`
                    }
                },
                data: daysCount > 2 && !!events.length ? range(0, daysCount, 1).map((dayNum) => ({
                    x: dayNum,
                    y: 80,
                    marker: {
                        enabled: false
                    }
                })) : []
            },
            {
                type: 'spline',
                name: 'Line',
                color: '#7BB5EC',
                lineWidth: 6,
                tooltip: {
                    headerFormat: '',
                    pointFormatter(): any {
                        // @ts-ignore
                        return `<b>${this.name}</b><br />${this.custom.start} - ${this.custom.end}`
                    }
                },
                marker: {
                    symbol: 'circle',
                    radius: 6
                },
                data: [
                    ...transformEventsToSeriesData(events, firstDay, daysCount)
                ]
            }
        ]
    }), [containerHeight, width, firstDay, cycles, events, daysCount]);

    const containerProps: SimpleMap<any> = useMemo(() => {
        return {
            style: { flex: 3, minWidth: 100 }
        };
    }, []);

    const reflowChart = useCallback((width?: number, height?: number): void => {
        if (height) {
            setHeight(height);
        }
        if (chartRef !== null && chartRef.current !== null) {
            chartRef.current.chart.reflow();
        }
    }, [setHeight, chartRef]);

    const handleCloseCard = useCallback(() => {
        setActiveCycle(null);
    }, []);

    return (
        <div className={styles.chart}>
            <Row style={{height : "calc(100vh - 104px)", width: "100%"}}>
                <Col span={!!activeCycle ? 17 : 24}>
                    <ReactResizeDetector
                        handleWidth
                        handleHeight
                        onResize={reflowChart}
                    />
                    <EmptyLoader loading={loading}>
                        <HighchartsReact
                            ref={chartRef}
                            highcharts={Highcharts}
                            options={options}
                            containerProps={containerProps}
                            className={styles.chart__highcharts}
                        />
                    </EmptyLoader>
                </Col>
                <Col span={!!activeCycle ? 7 : 0} style={{ height: '100%' }}>
                    {activeCycle && <CardComponent sprint={activeCycle} onClose={handleCloseCard} />}
                </Col>
            </Row>


        </div>
    );
});

export default Chart;
