import moment from "moment";
import {Module} from "../../models/module.model";

export const MODULES: Module[] = [
    {
        number: 1,
        startDate: moment('01.09.2020', 'DD.MM.YYYY'),
        endDate: moment('18.10.2020', 'DD.MM.YYYY'),
    },
    {
        number: 2,
        startDate: moment('01.09.2020', 'DD.MM.YYYY'),
        endDate: moment('18.10.2020', 'DD.MM.YYYY'),
    },
    {
        number: 3,
        startDate: moment('01.09.2020', 'DD.MM.YYYY'),
        endDate: moment('18.10.2020', 'DD.MM.YYYY'),
    },
    {
        number: 4,
        startDate: moment('01.09.2020', 'DD.MM.YYYY'),
        endDate: moment('18.10.2020', 'DD.MM.YYYY'),
    }
]
