import React, {memo, ReactNode} from "react";
import styles from './empty-loader.module.less';
import {Spin} from "antd";

interface EmptyLoaderProps {
    readonly loading: boolean;
    readonly children: ReactNode | ReactNode[];
}

const EmptyLoader = memo<EmptyLoaderProps>(({ children, loading }) => {
    return (
        <div className={styles.emptyLoader}>
            {loading ? (
                <div className={styles.emptyLoader__container}>
                    <Spin size="large" />
                </div>
            ) : (children)}
        </div>
    );
});

export default EmptyLoader;
