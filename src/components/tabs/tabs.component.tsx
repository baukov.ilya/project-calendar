import React, {memo, useEffect} from "react";
import styles from "./tabs.module.less"
import {Menu} from 'antd';
import EmptyLoader from "../common/empty-loader/empty-loader.component";


interface TabsComponentProps {
    loading: boolean,
    googleLists: any[],
    fetchCalendar: (pageNumber: number) => void
}

const TabsComponent = memo<TabsComponentProps>(({loading,googleLists, fetchCalendar}) => {
    useEffect(() => {
        fetchCalendar(1);
    }, []);
    return (
        <EmptyLoader loading={loading}>
            <Menu className={styles.menu} defaultSelectedKeys={['1']}
                  style={{boxShadow: "rgb(34 60 80 / 20%) -1px 0px 10px 2px", width: "90%"}}>
                <Menu.ItemGroup>
                    {googleLists.map((item: any) => {
                        return (
                            <Menu.Item
                                key={item.number + 1}
                                onClick={() => {fetchCalendar(item.number + 1)}}
                            >
                                {item.item}
                            </Menu.Item>
                        )
                    })}
                </Menu.ItemGroup>
            </Menu>
        </EmptyLoader>
    );
});

export default TabsComponent;
