import React from 'react';
import ReactDOM from 'react-dom';
import './index.less';
import * as serviceWorker from './serviceWorker';
import App from './components/app/app.component';
import { Provider } from 'react-redux';
import { Persistor, store } from './store/store';
import { PersistGate } from 'redux-persist/integration/react';

ReactDOM.render(
    <Provider store={store}>
        <PersistGate loading={null} persistor={Persistor}>
            <App />,
        </PersistGate>
    </Provider>,
    document.getElementById('root')
);

serviceWorker.unregister();
