import {Moment} from "moment";

export interface Event {
    readonly name: string;
    readonly startDate: Moment;
    readonly endDate: Moment;
}
