import {Moment} from "moment";

export interface Cycle {
    readonly number: number;
    readonly startDate: Moment;
    readonly endDate: Moment;
    readonly status: CycleStatus;
    readonly planningResults?: string;
    readonly actualResults?: string;
}

export type CycleStatus = 'Зачтен' | 'Не зачтен' | 'Текущий' | 'Не начат' | 'Каникулы';
