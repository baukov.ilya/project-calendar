import {Moment} from "moment";

export interface Module {
    readonly number: number;
    readonly startDate: Moment;
    readonly endDate: Moment;
}
