const express = require("express");

const app = express();
app.get("*", express.static("./build"));

const port = process.env.PORT || 8080;

app.listen(port, () => {
    console.log(`Project is running at http://localhost:${port}`);
});
